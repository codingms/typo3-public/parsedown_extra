# Parsedown Extra Change-Log

## 2024-07-08 Release of version 3.0.5

*	[TASK] Migrate to TYPO3 13, remove support for TYPO3 11
*	[TASK] Migrate TypoScript imports



## 2024-07-08 Release of version 3.0.5

*	[BUGFIX] Fix PHP parent class issue by updating ParseDown-Extra library
*	[TASK] Optimize version conditions in PHP code
*	[TASK] Code style optimization



## 2023-11-01 Release of version 3.0.4

*	[TASK] Clean up documentation
*	[BUGFIX] Fix get content object for TYPO3 11



## 2023-10-27  Release of version 3.0.3

*	[BUGFIX] Fix access on non defined ctype index in backend preview



## 2023-08-18  Release of version 3.0.2

*	[TASK] Migration of tt_content_drawItem hook for TYPO3 12



## 2023-08-16  Release of version 3.0.1

*	[TASK] Add more documentation



## 2023-08-08  Release of version 3.0.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10



## 2022-11-29 Release of Version 2.3.2

*	[BUGFIX] Fix insecure js library loading



## 2022-11-20 Release of Version 2.3.1

*	[BUGFIX] Fix attributes and classes for HTML table rendering



## 2022-11-07 Release of Version 2.3.0

*	[FEATURE] Insert configuration for image tag attributes
*	[BUGFIX] Fix PHP warnings for PHP 8.0



## 2022-09-09 Release of Version 2.2.0

*	[TASK] Upgrade Parsedown Extra Plugin



## 2022-08-18 Release of Version 2.1.3

*	[TASK] Optimize documentation metadata



## 2022-05-09 Release of Version 2.1.2

*	[BUGFIX] Fix selected markdown item in backend flexform
*	[TASK] Clean up ext_tables.php
*	[TASK] Update of the parsedown/extra/plugin libraries
*	[TASK] Add missing documentation file for styleguide



## 2022-04-09 Release of Version 2.1.1

*	[TASK] Move backend icon into Resources/Public/Icons
*	[TASK] Add content object into Fluid template



## 2022-02-07 Release of Version 2.1.0

*	[TASK] Insert a Styleguide Documentation page
*	[TASK] Code clean up
*	[TASK] Extend and validate documentations configuration
*	[TASK] Add documentations configuration
*	[TASK] Drop support for TYPO3 9
*	[TASK] Rise PHP version to 7.4
*	[TASK] Preparations for TYPO3 11



## 2021-02-19 Release of Version 2.0.2

*	[BUGFIX] Handle only relative files in markdown selection



## 2021-02-18 Release of Version 2.0.1

*	[BUGFIX] Fix file selection in content element



2021-02-14 Release of Version 2.0.0

*	[TASK] Migration for TYPO3 10



2021-01-07 Release of Version 1.5.3

*	[TASK] Add extra tags in composer.json
*	[TASK] Add german translation



## 2020-11-13 Release of Version 1.5.2

*	[BUGFIX] Check if markdown file exists in parsedown ViewHelper



## 2020-11-13 Release of Version 1.5.1

*	[TASK] Change extra tags



## 2020-11-13 Release of Version 1.5.0

*	[TASK] Disable Record storage page and Recursive in tt_content
*	[FEATURE] Viewhelper file attribute



## 2020-06-25 Release of Version 1.4.2

*	[TASK] Update extra tags in composer.json
*	[TASK] Update description in ext_emconf.php




## 2020-06-04 Release of Version 1.4.1

*	[TASK] Add extra tags in composer.json
*	[BUGFIX] Fix version in ext_emconf.php



## 2020-05-25 Release of Version 1.4.0

*	[TASK] Cleanup Change-Log



## 2020-01-13  Release of version 1.3.0

*	[FEATURE] Add parameter for shift HTML headlines
*	[FEATURE] Add parameter for disable email protection



## 2019-11-01  Release of version 1.2.2

*	[TASK] Add Gitlab-CI configuration.



## 2019-11-01  Release of version 1.2.1

*	[BUGFIX] Fix warning in TCA overrides.
*	[TASK] Changing links inside documentation.
*	[TASK] Remove DEV identifier.
*	[BUGFIX] Fixing argument type in Parsedown ViewHelper.



## 2019-01-21  Release of version 1.2.0

*	[TASK] Migration for TYPO3 9.5.



## 2018-10-24  Release of version 1.1.2

*	[BUGFIX] Fixing pass configuration from TypoScript.



## 2018-10-10  Release of version 1.1.1

*	[BUGFIX] Fixing array iteration of abbreviations.



## 2018-10-03  Release of version 1.1.0

*	[TASK] Moving abbreviations configuration into TypoScript settings.
*	[FEATURE] Adding configuration for CSS classes in HTML table tags.
*	[FEATURE] Adding configuration for Link attributes in HTML a tags.
*	[TASK] Working on documentation.
*	[TASK] Removing FlexForm and using DB-Field for selected Markdown files.
*	[TASK] Moving JavaScript into Footer
*	[FEATURE] Add page module preview
*	[TASK] Add icon as SVG
*	[FEATURE] Add parseing for email links with TYPO3 encryption
*	[TASK] Add a parse ViewHelper



## 2017-06-25  Release of version 1.0.0

*	[TASK] Development of first version

