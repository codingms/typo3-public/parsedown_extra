# Parsedown-Extra Extension for TYPO3

Render Markdown by using the Parsedown-Extra library in TYPO3.


**Features:**

*   Parse email links with TYPO3 encryption
*   Content-Element for selecting a Markdown file to parse and display the output
*   ViewHelper for parsing Markdown from individual sources
*   Defining abbreviations globally by using TypoScript
*   SourceCode syntax highlighting by using PrismJS
*   Syntax highlight for TypoScript

If you need some additional or custom feature - get in contact!


**Links:**

*   [Parsedown-Extra Documentation](https://www.coding.ms/documentation/typo3-parsedown-extra "Parsedown-Extra Documentation")
*   [Parsedown-Extra Bug-Tracker](https://gitlab.com/codingms/typo3-public/parsedown_extra/-/issues "Parsedown-Extra Bug-Tracker")
*   [Parsedown-Extra Repository](https://gitlab.com/codingms/typo3-public/parsedown_extra "Parsedown-Extra Repository")
*   [TYPO3 Parsedown-Extra Productdetails](https://www.coding.ms/de/typo3-extensions/typo3-parsedown-extra/ "TYPO3 Parsedown-Extra Productdetails")
*   [TYPO3 Parsedown-Extra Documentation](https://www.coding.ms/documentation/typo3-parsedown-extra "TYPO3 Parsedown-Extra Documentation")
*   [Markdown Syntax](https://daringfireball.net/projects/markdown/syntax "Markdown Syntax")
*   [Parsedown Website](http://parsedown.org/ "Parsedown Website")
*   [Parsedown-Extra Website](https://michelf.ca/projects/php-markdown/extra/ "Parsedown-Extra Website")
