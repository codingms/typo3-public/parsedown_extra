<?php

declare(strict_types=1);

namespace CodingMs\ParsedownExtra\EventListener;

use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;


final class DrawItem
{

    public function __invoke(PageContentPreviewRenderingEvent $event): void
    {
        if ($event->getTable() !== 'tt_content') {
            return;
        }

        if (isset($event->getRecord()['CType'])
            && $event->getRecord()['CType'] === 'list'
            && $event->getRecord()['list_type'] === 'parsedownextra_parsedown') {
            $languageService =GeneralUtility::makeInstance(LanguageServiceFactory::class) ->createFromUserPreferences($GLOBALS['BE_USER']);
            $lllFile = 'LLL:EXT:parsedown_extra/Resources/Private/Language/locallang_db.xlf:';
            $itemContent = '';
            $itemContent .= '<table class="table table-bordered table-striped">';
            if (trim($event->getRecord()['header']) !== '') {
                $itemContent .= $this->getTableRow($languageService->sL($lllFile . 'tx_parsedownextra_label.header'), $event->getRecord()['header']);
            }
            $markdownFile = $event->getRecord()['tx_parsedownextra_file'];
            $markdownFileAbsolute = GeneralUtility::getFileAbsFileName($markdownFile);
            if (!file_exists($markdownFileAbsolute)) {
                $message = $languageService->sL($lllFile . 'tx_parsedownextra_message.warning_parsedown_file_not_found');
                $markdownFile = '<span class="text-danger">' . $message . ' (\'' . $markdownFile . '\')</span>';
            }
            $itemContent .= $this->getTableRow($languageService->sL($lllFile . 'tx_parsedownextra_label.markdown_file'), $markdownFile);
            $itemContent .= '</table>';

            $event->setPreviewContent($itemContent);

        }
    }

    /**
     * @param string $label
     * @param mixed $value
     * @return string
     */
    protected function getTableRow($label, $value)
    {
        $itemContent = '';
        $itemContent .= '<tr>';
        $itemContent .= '<th>' . $label . '</th>';
        if (is_array($value)) {
            $itemContent .= '<td>' . implode(', ', $value) . '</td>';
        } else {
            $itemContent .= '<td>' . $value . '</td>';
        }
        $itemContent .= '</tr>';
        return $itemContent;
    }

}
