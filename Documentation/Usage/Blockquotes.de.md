# Block-Zitate

## Notizen, Warnungen etc. mit Hilfe von Block Zitaten

Warnungen, Notizen, Information und weitere können wie folgt angezeigt werden:

```markdown
>	#### Information: {.alert .alert-info}
>
>	This is the message...
```

Weil wir in unserem Frontend-Template Bootstrap-Styles verwenden, können wir *success*, *danger*, *warning* und *info* verwenden.



## Examples


### Info message box

>	#### Information: {.alert .alert-info}
>
>	This is the message...


### Error message box

>	#### Attention: {.alert .alert-danger}
>
>	This is the message...
