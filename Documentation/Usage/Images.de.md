# Bilder in Parsedown-Extra für TYPO3

Bild-Definitionen als Referenz:

```markdown
[module-filelist]: https://www.domain.de/fileadmin/Documentation/Images/Module/filelist.png "Dateilisten-Modul"
```

Verwendung der Bild-Referenz:

```markdown
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
![Filelist-Module][module-filelist] {.inline-image}
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
```
