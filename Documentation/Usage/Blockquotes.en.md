# Blockquotes

## Notices, Warnings and more by using blockquotes

Warnings, notices, information and more can be displayed like this:

```markdown
>	#### Information: {.alert .alert-info}
>
>	This is the message...
```

Because we're applying the Bootstrap-Styles, you can set *success*, *danger*, *warning* and *info*.



## Examples


### Info message box

>	#### Information: {.alert .alert-info}
>
>	This is the message...


### Error message box

>	#### Attention: {.alert .alert-danger}
>
>	This is the message...

