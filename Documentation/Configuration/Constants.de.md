# TypoScript-Konstanten Einstellungen


## Allgemein

| **Konstante**    | styles.content.imgtext.linkWrap.lightboxEnabled                |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | LightBox für Inhaltselemente/Bilder aktivieren                 |
| **Beschreibung** |                                                                |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                  |
| **Standardwert** | 1                                                              |

| **Konstante**    | styles.content.imgtext.linkWrap.lightboxCssClass               |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | CSS-Klasse für Lightbox                                        |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | lightbox                                                       |

| **Konstante**    | styles.content.imgtext.linkWrap.lightboxRelAttribute           |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | CSS-Relationattribut für Lightbox                              |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | lightbox[{field:uid}]                                          |

| **Konstante**    | themes.configuration.lightbox.colorbox.theme                   |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | LightBox-Design, mögliche Werte 1-5                            |
| **Beschreibung** |                                                                |
| **Typ**          | Auswahlbox mit Optionen: 1, 2, 3, 4, 5                         |
| **Standardwert** | 2                                                              |

| **Konstante**    | themes.configuration.lightbox.colorbox.cssUrl                  |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Basis-URL für CSS-Theme                                        |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/ |

| **Konstante**    | themes.configuration.lightbox.colorbox.jsUrl                   |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Basis-URL für Skript                                           |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/ |



