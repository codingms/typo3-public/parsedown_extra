# TypoScript constant settings


## General

| **Constant**      | styles.content.imgtext.linkWrap.lightboxEnabled                |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Enable LightBox for content elements/images                    |
| **Description**   |                                                                |
| **Type**          | Selectbox with options: 0, 1                                   |
| **Default value** | 1                                                              |

| **Constant**      | styles.content.imgtext.linkWrap.lightboxCssClass               |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | CSS class for lightbox                                         |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | lightbox                                                       |

| **Constant**      | styles.content.imgtext.linkWrap.lightboxRelAttribute           |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | CSS relation attribute for lightbox                            |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | lightbox[{field:uid}]                                          |

| **Constant**      | themes.configuration.lightbox.colorbox.theme                   |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | LightBox theme, possible values 1-5                            |
| **Description**   |                                                                |
| **Type**          | Selectbox with options: 1, 2, 3, 4, 5                          |
| **Default value** | 2                                                              |

| **Constant**      | themes.configuration.lightbox.colorbox.cssUrl                  |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Base url for CSS theme                                         |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/ |

| **Constant**      | themes.configuration.lightbox.colorbox.jsUrl                   |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Base url for Script                                            |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/ |



