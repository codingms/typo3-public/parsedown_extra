<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// register svg icons: identifier and filename
$iconsSvg = [
    'content-plugin-parsedownextra-parsedown' => 'Resources/Public/Icons/Extension.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        ['source' => 'EXT:parsedown_extra/' . $path]
    );
}
