<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

// Parsedown plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'ParsedownExtra',
    'Parsedown',
    [\CodingMs\ParsedownExtra\Controller\ParsedownController::class => 'show'],
    []
);
$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('parsedown_extra');
if (!class_exists('\Parsedown')) {
    $library = '/Resources/Private/Php/Parsedown/Parsedown.php';
    include_once($extensionPath . $library);
}
if (!class_exists('\ParsedownExtra')) {
    $library = '/Resources/Private/Php/ParsedownExtra/ParsedownExtra.php';
    include_once($extensionPath . $library);
}
if (!class_exists('\ParsedownExtraPlugin')) {
    $library = '/Resources/Private/Php/ParsedownExtraPlugin/ParsedownExtraPlugin.php';
    include_once($extensionPath . $library);
}
// Page TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '@import "EXT:parsedown_extra/Configuration/PageTS/tsconfig.typoscript"'
);
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1586262363] = [
    'nodeName' => 'markdownSelect',
    'priority' => '70',
    'class' => \CodingMs\ParsedownExtra\Form\Element\MarkdownSelect::class
];
