# Parsedown-Extra Erweiterung für TYPO3

Rendert Markdown mit Hilfe der Parsedown-Extra Bibliothek in TYPO3.


**Features:**

*   Parst E-Mailadressen Links mit der TYPO3 encryption
*   Inhaltselement zur Auswahl von Markdown-Dateien, welche geparst im Frontend ausgegeben werden
*   ViewHelper zum parsen von Markdown aus beliebigen Quellen
*   Global definiertes Abkürzungs-Verzeichnis aus TypoScript
*   SourceCode Syntax-Highlighting mit PrismJS
*   Syntax-Highlighting für TypoScript

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Parsedown-Extra Dokumentation](https://www.coding.ms/documentation/typo3-parsedown-extra "Parsedown-Extra Dokumentation")
*   [Parsedown-Extra Bug-Tracker](https://gitlab.com/codingms/typo3-public/parsedown_extra/-/issues "Parsedown-Extra Bug-Tracker")
*   [Parsedown-Extra Repository](https://gitlab.com/codingms/typo3-public/parsedown_extra "Parsedown-Extra Repository")
*   [TYPO3 Parsedown-Extra Productdetails](https://www.coding.ms/typo3-extensions/typo3-parsedown-extra/ "TYPO3 Parsedown-Extra Productdetails")
*   [TYPO3 Parsedown-Extra Dokumentation](https://www.coding.ms/de/dokumentation/typo3-parsedown-extra "TYPO3 Parsedown-Extra Dokumentation")
*   [Markdown Syntax](https://daringfireball.net/projects/markdown/syntax "Markdown Syntax")
*   [Parsedown Webseite](http://parsedown.org/ "Parsedown Webseite")
*   [Parsedown-Extra Webseite](https://michelf.ca/projects/php-markdown/extra/ "Parsedown-Extra Webseite")
