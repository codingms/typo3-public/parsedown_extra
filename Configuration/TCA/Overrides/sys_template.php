<?php

//
// Static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'parsedown_extra',
    'Configuration/TypoScript/Stylesheet',
    'Parsedown - Default stylesheets'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'parsedown_extra',
    'Configuration/TypoScript/Colorbox',
    'Parsedown - Colorbox'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'parsedown_extra',
    'Configuration/TypoScript/PrismJs',
    'Parsedown - PrismJs Syntax-Highlighting'
);
