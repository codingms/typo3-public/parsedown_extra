<?php

//
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'parsedown_extra',
    'Parsedown',
    'Parsedown (Markdown)'
);
//
$field = [
    'tx_parsedownextra_file' => [
        'displayCond' => [
            'AND' => [
                'FIELD:CType:=:list',
                'FIELD:list_type:=:parsedownextra_parsedown',
            ],
        ],
        'label' => 'LLL:EXT:parsedown_extra/Resources/Private/Language/locallang_db.xlf:tx_parsedownextra_label.markdown_file',
        'exclude' => 0,
        'config' => [
            'type' => 'user',
            'renderType' => 'markdownSelect',
        ],
    ],
];

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['parsedownextra_parsedown']='pages, recursive';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $field);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'tx_parsedownextra_file',
    '',
    'after:list_type'
);
